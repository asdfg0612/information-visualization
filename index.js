const height = window.innerHeight * 0.85,
      width = window.innerWidth,
      padding = height*0.05;

let inTown = Boolean(false),
    lastYear = 'none',
    nowYear = 'none',
    countyName = 'none';

let years = ["104", "105", "106", "107", "108", "109"],
    months = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];

// console.log(height);
// console.log(width);

/* map部分 */
let mapHeight = height*0.8,
    mapWidth = width * 1.8 / 5;


let svg = d3.select('#map_rect')
            .append('svg')
            .style('height', mapHeight)
            .style('width', mapWidth);

let svg_map = svg.append("g");

let tooltip = d3.select('#tooltip')
                .style('position', 'absolute')
                .style('background', 'grey')
                .style('width', 130)
                .style('height', 40)
                .style('display', 'none');

d3.select('#map_rect').on('mousemove', function(e){
                          tooltip.style('left', e.layerX*1.2)
                                 .style('top', e.layerY*1.2)
                      });

let json_path_country = 'https://raw.githubusercontent.com/FrogL0612/Vis_final/main/topo_map/COUNTY_MOI_1090820.json',
    json_path_town = 'https://raw.githubusercontent.com/FrogL0612/Vis_final/main/topo_map/TOWN_MOI_1100415.json',
    crime_data = 'https://raw.githubusercontent.com/FrogL0612/Vis_final/main/json_data/crime_filtered.csv';

function load_data(amount_path) {
    return new Promise(function (resolve, reject) {
        resolve(d3.csv(amount_path))
    });
}

function load_country_map(json_path) {
    return new Promise(function (resolve, reject) {
        d3.json(json_path).then((data) => {
            resolve(topojson.feature(data, data.objects.COUNTY_MOI_1090820).features)
        });
    });
}

function load_town_map(json_path) {
    return new Promise(function (resolve, reject) {
        d3.json(json_path).then((data) => {
            resolve(topojson.feature(data, data.objects.TOWN_MOI_1100415).features)
        });
    });
}

function draw_map(data, counties, towns) {
    // console.log(towns)
    let projection = d3.geoMercator()
                       .center([121.4, 24.3])
                       .scale(height*12)
                       .translate([width / 4.5, height / 4]);

    let map_path = d3.geoPath()
                     .projection(projection);

    let max_amount = 17000
    if (nowYear !== 'none') {
        max_amount = 4000
    }

    let color = d3.scaleSequential(d3.interpolateYlGn)
                  .domain([0, max_amount]);

    let TOWNcolor = d3.scaleSequential(d3.interpolateYlGn)
                      .domain([0, 2000]);

    let countryPaths = svg_map.selectAll('.geo-path')
                              .data(counties)
                              .join('path')
                              .attr('class', 'geo-path')
                              .attr('d', map_path)
                              .style('fill', d=>{
                                  let Name = d.properties.COUNTYNAME;
                                  // console.log(countyName)
                                  crime_total = data.filter(function(d) {
                                      if (nowYear === 'none') {
                                          return d.county === Name
                                      } else {
                                          return d.county === Name && d.year === nowYear
                                      }
                                  })
                                  // console.log(crime_total.length)

                                  if (crime_total.length === 0){
                                       return 'lightgrey'
                                  } else {
                                      return color(crime_total.length)
                                  }
                              })
                              .on('mouseover', function(e){
                                  d3.select(this)
                                    .style('stroke', 'white')
                                  d3.select(this)
                                    .select(function(d){
                                        let Name = d.properties.COUNTYNAME
                                        // console.log(countyName)
                                        crime_total = data.filter(function(d) {
                                            if (nowYear === 'none') {
                                                return d.county === Name
                                            } else {
                                                return d.county === Name && d.year === nowYear
                                            }
                                        })
                                        // console.log(crime_total.length)

                                        if (crime_total.length === 0) {
                                            tooltip.select('text').html('暫無資料')

                                            tooltip.style('display', 'block')
                                        } else {
                                            tooltip.select('text').html(d.properties.COUNTYNAME + ' : ' + crime_total.length + '件')

                                            tooltip.style('display', 'block')
                                        }
                                    })
                              })
                              .on('mouseleave', function(e){
                                  d3.select(this).style('stroke', 'none')

                                  tooltip.style('display', 'none')
                              })
                              .on('click', function () {
                                  d3.select(this)
                                    .select(function(d) {
                                        let Name = d.properties.COUNTYNAME;
                                        countyName = d.properties.COUNTYNAME;
                                        let crime_data = data.filter(function (d) {
                                            return d.county === Name
                                        })
                                        // console.log(crime_data)

                                        updata_chart(crime_data);

                                        if (nowYear === 'none') {
                                            d3.select('.map_title')
                                                .text('全國 ')
                                        } else {
                                            d3.select('.map_title')
                                                .text('全國 ' + nowYear + ' ')
                                        }
                                    });
                              })
                              .on('dblclick ', function(d){
                                  d3.select(this)
                                    .select(function(d){
                                        if (inTown){
                                            inTown = false
                                            TownZoom()
                                        }else{
                                            inTown = true
                                            countryZoom(d.properties.COUNTYID)
                                        }
                                  })

                                  if (nowYear === 'none') {
                                      d3.select('.map_title')
                                          .text(countyName + ' ')
                                  } else {
                                      d3.select('.map_title')
                                          .text(countyName + ' ' + nowYear + ' ')
                                  }
                              });

    function TownZoom() {
        if (inTown) {
            countryZoom(d.properties.COUNTYID);
        }
        let t = d3.transition().duration(800)

        projection.scale(height * 12)
                  .translate([width / 4.5, height / 4])

        countryPaths.transition(t)
                    .attr('d', map_path)
                    .style('fill', function (d) {
                        let Name = d.properties.COUNTYNAME;
                        // console.log(countyName)
                        crime_total = data.filter(function(d) {
                            if (nowYear === 'none') {
                                return d.county === Name
                            } else {
                                return d.county === Name && d.year === nowYear
                            }
                        })
                        // console.log(crime_total.length)

                        if (crime_total.length === 0){
                            return 'lightgrey'
                        } else {
                            return color(crime_total.length)
                        }
                    });

        d3.select('#max_num')
            .text(max_amount)

        d3.select('#min_num')
            .text("0")

        svg.selectAll('.county')
           .data([])
           .exit().transition(t)
           .attr('d', map_path)
           .style('opacity', 0)
           .remove()
    }

    function countryZoom(id) {

        let country = counties.find(function (d) { return d.properties.COUNTYID === id })
        let countryTowns = towns.filter(function (d) {
            return d.properties.COUNTYID === id
        })


        let t = d3.transition()
                  .duration(800);

        let townPaths = svg.selectAll('.town')
                           .data(countryTowns, function (d) {
                               return d.properties.COUNTYID
                           });


        let enterTownPaths = townPaths.enter()
                                      .append('path')
                                      .attr('class', 'county')
                                      .attr('d', map_path)
                                      .style('fill', function (d) {
                                          townName = d.properties.TOWNNAME
                                          // console.log(townName)
                                          crime_total = data.filter(function(d) {
                                              if (nowYear === 'none') {
                                                  return d.district === townName
                                              } else {
                                                  return d.district === townName && d.year === nowYear
                                              }
                                          })
                                          // console.log(crime_total.length)
                                          if (crime_total.length === 0){
                                              return 'lightgrey'
                                          } else {
                                              return TOWNcolor(crime_total.length)
                                          }
                                      })
                                      .style('opacity', 0)
                                      .on('mouseover', function(){
                                          d3.select(this)
                                            .style('stroke', 'white')
                                          d3.select(this)
                                            .select(function(d){
                                                  townName = d.properties.TOWNNAME
                                                  // console.log(townName)
                                                  crime_total = data.filter(function(d) {
                                                      if (nowYear === 'none') {
                                                          return d.district === townName
                                                      } else {
                                                          return d.district === townName && d.year === nowYear
                                                      }
                                                  })
                                                  // console.log(crime_total.length)

                                                  if (crime_total.length === 0) {
                                                      tooltip.select('text').html('暫無資料')
                                                      tooltip.style('display', 'block')
                                                  } else {
                                                      tooltip.select('text').html(d.properties.TOWNNAME + ' : ' + crime_total.length + '件')
                                                      tooltip.style('display', 'block')
                                                  }
                                            })
                                      })
                                      .on('mouseleave', function(){
                                          d3.select(this).style('stroke', 'none')

                                          tooltip.style('display', 'none')
                                      })
                                      .on('click', function () {
                                          let crime_data = data.filter(function(d) {
                                              return d.district === townName
                                          })
                                          countyName = townName;
                                          updata_chart(crime_data);
                                      })
                                      .on('dblclick ', function () {
                                          inTown = false
                                          countyName = 'none'

                                          updata_chart(data)
                                          TownZoom()
                                      });

        projection.fitExtent(
            [[padding, padding], [width / 2.5 - padding, height - padding]],country
        );

        countryPaths.transition(t)
                    .attr('d', map_path)
                    .style('fill', '#444');

        enterTownPaths.transition(t)
                      .attr('d', map_path)
                      .style('opacity', 1);

        townPaths.exit()
                 .transition(t)
                 .attr('d', map_path)
                 .style('opacity', 0)
                 .remove();

        d3.select('#max_num')
            .text("2000");

        d3.select('#min_num')
            .text("0");
    }

    d3.select('#max_num')
      .text(max_amount);

    d3.select('#min_num')
        .text("0");

    d3.select("#year_all")
        .on("click", function(){
            lastYear = nowYear;
            nowYear = 'none';
            buttom_click(data)
        });
    d3.select("#year_2020")
        .on("click", function(){
            lastYear = nowYear;
            nowYear = '109';
            buttom_click(data)
        });
    d3.select("#year_2019")
        .on("click", function(){
            lastYear = nowYear;
            nowYear = '108';
            buttom_click(data)
        });
    d3.select("#year_2018")
        .on("click", function(){
            lastYear = nowYear;
            nowYear = '107';
            buttom_click(data)
        });
    d3.select("#year_2017")
        .on("click", function(){
            lastYear = nowYear;
            nowYear = '106';
            buttom_click(data)
        });
    d3.select("#year_2016")
        .on("click", function(){
            lastYear = nowYear;
            nowYear = '105';
            buttom_click(data)
        });
    d3.select("#year_2015")
        .on("click", function(){
            lastYear = nowYear;
            nowYear = '104';
            buttom_click(data)
        });
}

function buttom_click(data){
    let crime_data = data.filter(function(d) {
        if (countyName === 'none') {
            return d;
        } else{
            return d.county === countyName;
        }
    })
    updata_chart(crime_data);


    d3.select('#loading')
        .style('display', 'block');

    svg_map.selectAll('.geo-path')
        .remove();

    svg.selectAll('.county')
        .remove()

    updata_map();
}


async function initiate(data_path, json_path_country, json_path_town) {
    try{
        let data = await load_data(data_path);
        console.log('讀取完CSV資料')

        let map_country = await load_country_map(json_path_country),
            map_town = await load_town_map(json_path_town);
        console.log('讀取完topology資料')

        d3.select('#loading')
          .style('display', 'none');

        draw_map(data, map_country, map_town);

        draw_line(data);
        draw_bar(data);
        draw_dount(data);

    }catch(e){
        console.log(e);
    }
}


const PieWidth = width / 5,
      PieHeight = PieWidth,
      PieRadius = Math.min(PieWidth*0.9, PieHeight*0.9) / 2,
      DonutWidth = PieWidth / 4.8;

const legendRectSize = 13,
      legendSpacing = 7;

let PieColor = d3.scaleOrdinal()
                 .range(["#42bfac", "#2c80c5", "#ffbf00"]);

let crime_type = ['住宅竊盜', '汽車竊盜', '機車竊盜']


function construct_dounut_data(data) {
    if (nowYear === 'none') {
        return search_data(data);
    } else {
        let total_data = data.filter( function(d) {
            return d.year === nowYear;
        });
        return search_data(total_data);
    }

    function search_data(total_data) {
        let ret_data = [];
        crime_type.forEach(crime => {
            let crime_value = total_data.filter( function(d) {
                return d.type === crime;
            });
            ret_data.push({
                type: crime,
                value: crime_value.length,
                total: total_data.length
            });
        });
        return ret_data;
    }
}

// 生成圓餅圖畫布
let DonutSvg = d3.select('#donut')
                 .append('svg')
                 .attr("id", "donut_Svg")
                 .attr('width', PieWidth)
                 .attr('height', PieHeight)
                 .append('g')
                 .attr('transform', 'translate(' + (PieWidth / 2) + ',' + (PieHeight / 2) + ')');

function draw_dount(data) {
    data = construct_dounut_data(data);
    // console.log(data)

    let pie = d3.pie()
        .value(function (d) {
            return d.value;
        })
        .sort(null);

    let arc = d3.arc()
        .innerRadius(PieRadius - DonutWidth)
        .outerRadius(PieRadius);

    DonutSvg.selectAll('path')
        .data(pie(data))
        .enter()
        .append('path')
        .attr('d', arc)
        .attr('fill', function (d) {
            return PieColor(d.data.type);
        })
        .attr('transform', 'translate(0, 0)')
        .on('mouseover', function () {
            d3.select(this).transition()
                .duration('50')
                .attr('opacity', '.85');
        })
        .on('mouseout', function () {
            d3.select(this).transition()
                .duration('50')
                .attr('opacity', '1');
        });

    let legend = DonutSvg.selectAll('.legend')
                         .data(PieColor.domain())
                         .enter()
                         .append('g')
                         .attr('class', 'circle-legend')
                         .attr('transform', function (d, i) {
                             // console.log(d)
                             let LegendHeight = legendRectSize + legendSpacing,
                                 LegendOffset = LegendHeight * PieColor.domain().length / 2,
                                 LegendHorz = -2 * legendRectSize - 13,
                                 LegendVert = i * LegendHeight - LegendOffset;
                             return 'translate(' + LegendHorz + ',' + LegendVert + ')';
                         });

    legend.append('circle')
          .style('fill', PieColor)
          .style('stroke', PieColor)
          .attr('cx', -15)
          .attr('cy', 0)
          .attr('r', '.5rem');

    legend.append('text')
          .attr('x', legendRectSize + legendSpacing-15)
          .attr('y', legendRectSize - legendSpacing)
          .text(function (d) {
              numData = data.filter(function(f) {
                  return f.type === d;
              })
              let num = (Math.round((numData[0].value / numData[0].total) * 100)).toString() + '%';

              return d + ' : ' + num;
          });
}

function change_donut(newData) {
    let newPie = d3.pie()
                   .value(function (d) {
                       return d.value;
                   })
                   .sort(null)(newData);

    let newDonut = d3.select("#donut")
                     .selectAll("path")
                     .data(newPie);

    let newArc = d3.arc()
                   .innerRadius(PieRadius - DonutWidth)
                   .outerRadius(PieRadius);

    newDonut.transition()
            .duration(500)
            .attr("d", newArc);

    d3.select("#donut")
      .selectAll("text")
      .text(function (d) {
          let numData = newData.filter(function(f) {
              return f.type === d;
          });
          let num = (Math.round((numData[0].value / numData[0].total) * 100)).toString() + '%';
          return d + ' : ' + num;
      });
}


// 折線圖
let line_svg_h = height*0.6 - PieHeight*0.6 - 25,
    line_svg_w = width*0.9 - width *2/5,
    line_svg_name = 40;

// margin
let lineMargin = {top: 10, right: 30, bottom: 40, left: 60},
    lineWidth = line_svg_w - lineMargin.left - lineMargin.right,
    lineHeight = (line_svg_h - line_svg_name) - lineMargin.top - lineMargin.bottom;

let lineTip = d3.select("#line_chart")
                .append("div")
                .attr("class", "lineTip")
                .style("opacity", 0.0);

// 底層畫布
let lineSvg = d3.select("#line_chart")
                .append("svg")
                .attr("id", "line_Svg")
                .attr("width", line_svg_w)
                .attr("height", line_svg_h)
                .append("g")
                .attr("transform", "translate(" + (lineMargin.left + lineMargin.right) + "," + (lineMargin.top + lineMargin.bottom*0.5) + ")");


function construct_new_data(data) {
    if (nowYear === 'none') {
        return search_data(data, years, "年");
    } else {
        let total_data = data.filter( function(d) {
            return d.year === nowYear;
        });
        return search_data(total_data, months, "月");
    }

    function search_data(total_data, target, word) {
        let ret_data = [],
            money = 0;
        if (nowYear === 'none') {
            target.forEach(year => {
                let acc_years = total_data.filter( function(d) {
                    if (d.money !== '0' && d.year === year){
                        money = money + parseInt(d.money)
                        return d.year === year;
                    } else {
                        return d.year === year;
                    }
                });
                ret_data.push({
                    date: year + word,
                    amount: acc_years.length,
                    money: money
                });

                money = 0
            });
        } else {
            target.forEach(month => {
                let acc_months = total_data.filter( function(d) {
                    if (d.money !== '0' && d.month === month) {
                        money = money + parseInt(d.money)
                        return d.month === month;
                    } else {
                        return d.month === month;
                    }
                });

                ret_data.push({
                    date: month + word,
                    amount: acc_months.length,
                    money: money
                });
                money = 0;
            });
        }
        return ret_data;
    }
}

function draw_line(crime_data) {
    let data = construct_new_data(crime_data);

    // 產生x座標
    year = []
    data.forEach(d => {
        year.push(d["date"] )
    });

    // x-axis
    let line_x = d3.scalePoint()
                   .domain([...year])
                   .range([0, lineWidth*0.95]);

    // y-axis
    let line_y = d3.scaleLinear()
                   .domain([500, 30000])
                   .range([lineHeight*0.95, 0])

    lineSvg.append("g")
           .attr("id", "x_axis")
           .attr("transform", "translate(0," + lineHeight + ")")
           .call(d3.axisBottom(line_x))
           .selectAll("text")
           .attr("y", 20)
           .attr("x", 0)
           .attr("dy", ".40em")
           .attr('fill', '#2C363F')
           .style("font-size", "15px")

    lineSvg.append("g")
        .attr("id", "y_axis")
        .call(d3.axisLeft(line_y))
        .selectAll("text")
        .attr('fill', '#2C363F')
        .style("font-size", "15px")

    //x軸名
    lineSvg.append("g")
        .attr("transform", "translate(" + (lineWidth/2 - line_svg_name/3) + "," + (lineHeight + line_svg_name*1.7) + ")")
        .append("text")
        .attr('fill', '#2C363F')
        .text("時間")
        .style("font-weight", "bold")
        .style("font-size", "20px")
        .attr("y", -25)

    //y軸名
    lineSvg.append("g")
        .attr("transform", "translate(" + 0 + "," + lineHeight/1.8 + ")")
        .append("text")
        .attr('transform', 'rotate(-90)')
        .attr('fill', '#2C363F')
        .text("案量")
        .style("font-weight", "bold")
        .style("font-size", "20px")
        .attr("y", -60)

    // 畫線
    lineSvg.append('path')
        .datum(data)
        .attr("id", "data_line")
        .attr("d", d3.line()
            .x(function(d,i){return line_x(year[i])})
            .y(function(d,i){return line_y(d['amount'])})
        )
        .attr("fill", "none")
        .attr("stroke", "#FF5809")
        .attr("stroke-width", "2px")
        .attr("opacity", 1)

    // 畫點
    lineSvg.append("g")
        .selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr("class", "myCircle")
        .attr("cx", function(d,i){return line_x(year[i])})
        .attr("cy", function(d,i){return line_y(d['amount'])})
        .attr("r", 5)
        .attr("stroke", "#A23400")
        .attr("stroke-width", "3px")
        .attr("fill", "#FFBD9D")
        .attr("fill-opacity", 1)
        .on("mouseover", lineMouseover)
        .on("mouseleave", lineMouseleave)
}

// mouse functions
let lineMouseover = function(d,i) {
    d3.select(this).transition()
        .style('fill', '#00AEAE');

    lineTip.transition()
        .duration(200)
        .style("opacity", 0.9);

    lineTip.html("時間：" + i.date + "<br/>"+
        "件數："+i.amount+ "<br/>")
        .style("left", (width * 2.2/5 + event.layerX) + "px")
        .style("top", (height*0.55 + event.layerY) + "px")
        .style("opacity", 1.0)
}

let lineMouseleave = function() {
    d3.select(this).transition()
        .style('fill', '#FFBD9D');

    lineTip.transition()
        .style("opacity", 0);
}

// 控制年份

// line data
let update_line = function(data){
    let year = [],
        amount = [];

    data.forEach(d => {
        year.push(d["date"])
        amount.push(d["amount"])
    });

    // x-axis
    let line_x = d3.scalePoint()
        .domain([...year])
        .range([0, lineWidth*0.95])

    // y-axis
    let line_y = d3.scaleLinear()
        .domain([d3.min(amount)*0.8, d3.max(amount)*1.1])
        .range([lineHeight*0.95, 0])

    lineSvg.select('#x_axis')
            .attr("transform", "translate(0," + lineHeight + ")")
            .call(d3.axisBottom(line_x))
            .selectAll("text")
            .attr("y", 20)
            .attr("x", 0)
            .attr("dy", ".40em")
            .attr('fill', '#2C363F')
            .style("font-size", "15px")

    lineSvg.select("#y_axis")
           .call(d3.axisLeft(line_y))
           .selectAll("text")
           .attr('fill', '#2C363F')
           .style("font-size", "15px")

    if (lastYear === 'none' && nowYear !== 'none') {
        d3.selectAll(".myCircle")
          .remove()

        build_myCircle(amount);
    }

    if (lastYear !== 'none' && nowYear === 'none') {
        d3.selectAll(".myCircle")
            .remove()

        build_myCircle(amount);
    }

    d3.selectAll(".myCircle")
        .data(data)
        .transition()
        .duration(1000)
        .attr("cx", function(d,i){return line_x(year[i])})
        .attr("cy", function(d){return line_y(d["amount"])})
        .attr("stroke", "#F2E8CF")

    d3.select("#data_line")
        .datum(amount)
        .transition()
        .duration(1000)
        .attr("d", d3.line()
            .x(function(d,i){return line_x(year[i])})
            .y(function(d,i){return line_y(d)})
        )

    function build_myCircle(data) {
        lineSvg.selectAll(".myCircle")
            .data(data)
            .enter()
            .append("circle")
            .attr("class", "myCircle")
            .attr("cx", function(d,i){return line_x(year[i])})
            .attr("cy", function(d){return line_y(d)})
            .attr("r", 5)
            .attr("stroke", "#F2E8CF")
            .attr("stroke-width", "3px")
            .attr("fill", "white")
            .attr("fill-opacity", 1)
            .on("mouseover", lineMouseover)
            .on("mouseleave", lineMouseleave);
    }
}

// 長條圖
let bar_svg_h = PieHeight,
    bar_svg_w = width*0.9 - PieWidth - width *2/5,
    bar_svg_name = 40;

let barMargin = {top: 10, right: 130, bottom: 40, left: 120},
    barWidth = bar_svg_w - barMargin.left - barMargin.right,
    barHeight = (bar_svg_h - bar_svg_name) - barMargin.top - barMargin.bottom;


// 底層畫布
let barSvg = d3.select("#bar_chart")
               .append("svg")
               .attr("id", "bar_Svg")
               .attr("width", bar_svg_w)
               .attr("height", bar_svg_h)
               .append("g")
               .attr("transform", "translate(" + (barMargin.left+25) + "," + barMargin.top + ")")

function draw_bar(crime_data) {
    let data = construct_new_data(crime_data);

    // 產生x座標
    let year = [0],
        amount = [];

    data.forEach(d => {
        year.push(d["date"] )
        amount.push(d["money"])
    });

    // x-axis
    let bar_x = d3.scalePoint()
                  .domain([...year])
                  .range([0, barWidth])

    // y-axis
    let axis_y = d3.scaleLinear()
                   .domain([d3.min(amount)*0.8, d3.max(amount)*1.1])
                   .range([barHeight, 0])

    // height for bar
    let bar_y = d3.scaleLinear()
                  .domain([d3.min(amount)*0.8, d3.max(amount)*1.1])
                  .range([0, barHeight])

    barSvg.append("g")
          .attr("id", "x_axis")
          .attr("transform", "translate(0," + barHeight + ")")
          .call(d3.axisBottom(bar_x))
          .selectAll("text")
          .attr("y", 20)
          .attr("x", 0)
          .attr("dy", ".40em")
          .attr('fill', '#2C363F')
          .style("font-size", "15px")

    barSvg.append("g")
          .attr("id", "y_axis")
          .call(d3.axisLeft(axis_y))
          .selectAll("text")
          .attr('fill', '#2C363F')
          .style("font-size", "15px")

    //x軸名
    barSvg.append("g")
          .attr("transform", "translate(" + barWidth / 2 + "," + (bar_svg_h - bar_svg_name*0.2) + ")")
          .append("text")
          .attr('fill', '#2C363F')
          .text("時間")
          .style("font-weight", "bold")
          .style("font-size", "20px")
          .attr("y", -25)

    //y軸名
    barSvg.append("g")
          .attr("transform", "translate(" + 0 + "," + barHeight/1.8 + ")")
          .append("text")
          .attr('transform', 'rotate(-90)')
          .attr('fill', '#2C363F')
          .text("罰金")
          .style("font-weight", "bold")
          .style("font-size", "20px")
          .attr("y", -100)

    // 畫每一個bar
    barSvg.append('g')
          .selectAll(".bar")
          .data(amount)
          .enter()
          .append("rect")
          .attr("class", "bar_rect")
          .attr("fill", "#FF8040")
          .attr("x", (d, i) => bar_x(year[i + 1]) - 15)
          .attr("y", d => barHeight - bar_y(d))
          .attr("width", '30')
          .attr("height", d => bar_y(d))
}

let update_bar = function(data){

    let year = [0],
        amount = [];

    data.forEach(d => {
        year.push(d["date"] )
        amount.push(d["money"])
    });

    // x-axis
    let bar_x = d3.scalePoint()
                  .domain([...year])
                  .range([0, barWidth])

    // y-axis
    let axis_y = d3.scaleLinear()
                   .domain([d3.min(amount)*0.8, d3.max(amount)*1.1])
                   .range([barHeight, 0])

    // height for bar
    let bar_y = d3.scaleLinear()
                  .domain([d3.min(amount)*0.8, d3.max(amount)*1.1])
                  .range([0, barHeight])


    barSvg.select('#x_axis')
          .attr("transform", "translate(0," + barHeight + ")")
          .call(d3.axisBottom(bar_x))
          .selectAll("text")
          .attr("y", 20)
          .attr("x", 0)
          .attr("dy", ".40em")
          .attr('fill', '#2C363F')
          .style("font-size", "15px")


    barSvg.select("#y_axis")
          .call(d3.axisLeft(axis_y))
          .selectAll("text")
          .attr('fill', '#2C363F')
          .style("font-size", "15px")


    barSvg.selectAll(".bar_rect")
          .remove()

    barSvg.selectAll(".bar")
          .data(amount)
          .enter()
          .append("rect")
          .attr("class", "bar_rect")

    barSvg.selectAll(".bar_rect")
          .attr("fill", "#FF8040")
          .attr("x", (d, i) => bar_x(year[i + 1]) - 15)
          .attr("y", d => barHeight - bar_y(d))
          .attr("width", '30')
          .attr("height", d => bar_y(d))
}


async function updata_chart(data) {
    let newData = construct_new_data(data),
        donutData = construct_dounut_data(data);

    if(nowYear === "none"){
        if (countyName === "none") {
            d3.selectAll(".data_range")
              .text('全國 ');
            d3.selectAll(".map_title")
              .text('全國 ');
        } else if (inTown) {
            d3.selectAll(".data_range")
              .text(countyName + ' ');
        } else {
            d3.selectAll(".data_range")
                .text(countyName + ' ');
        }
    }else {
        if (countyName === "none") {
            d3.selectAll(".data_range")
                .text('全國 ' + nowYear + " ");
            d3.selectAll(".map_title")
                .text('全國 ' + nowYear + " ");
        } else if (inTown) {
            d3.selectAll(".data_range")
                .text(countyName + ' ' + nowYear + " ");
        } else {
            d3.selectAll(".data_range")
                .text(countyName + ' ' + nowYear + " ");
            d3.selectAll(".map_title")
                .text(countyName + ' ' + nowYear + " ");
        }
    }

    update_line(newData);
    update_bar(newData);
    change_donut(donutData);
}

async function updata_map() {
    let json_path_country = 'https://raw.githubusercontent.com/FrogL0612/Vis_final/main/topo_map/COUNTY_MOI_1090820.json',
        json_path_town = 'https://raw.githubusercontent.com/FrogL0612/Vis_final/main/topo_map/TOWN_MOI_1100415.json',
        data_path = 'https://raw.githubusercontent.com/FrogL0612/Vis_final/main/json_data/crime_filtered.csv';

    let data = await load_data(data_path);
    console.log('讀取完CSV資料')


    let map_country = await load_country_map(json_path_country),
        map_town = await load_town_map(json_path_town);
    console.log('讀取完topology資料')

    d3.select('#loading')
        .style('display', 'none');

    draw_map(data, map_country, map_town);
}

initiate(crime_data, json_path_country, json_path_town);